﻿using System;
using API.DTOs.Read;
using API.DTOs.Write;
using API.Models;
using AutoMapper;

namespace API.Mappers
{
    public class AutoMappers : Profile
    {
        public AutoMappers()
        {
            CreateMap<Animal, AnimalRead>().ReverseMap();
            CreateMap<AnimalWrite, Animal>() //paso un modelo de escritura para obtener un modelo de salida
                .ForMember(x => x.foto, k => k.Ignore()); //para ignorar atributos especiales
        }
    }
}
