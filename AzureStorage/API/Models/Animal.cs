﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class Animal
    {
        public Animal()
        {
            id = Guid.NewGuid().ToString();
        }
        [Key]
        public string id { get; set; }
        public string nombre { get; set; }
        public string foto { get; set; }
    }
}
