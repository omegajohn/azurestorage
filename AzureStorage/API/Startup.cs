﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.DML;
using API.Interfaces;
using API.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var mysql = Configuration.GetConnectionString("MySql");
            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));
            services.AddDbContextPool<DataContext>(x =>
                x.UseMySql(mysql, ServerVersion.AutoDetect(mysql)));
            services.AddScoped<IAnimal, AnimalService>();
            services.AddScoped<IAzureStorage, AzureService>();
            services.AddSwaggerGen(
            k => k.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "MyApi",
                Version = " v1",
                Description = "Des1"
            }));
            services.AddCors(x =>
                x.AddDefaultPolicy(builder => {
                    builder.AllowAnyOrigin()
                    .AllowAnyMethod().AllowAnyHeader();
                }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors();

            app.UseSwagger();

            app.UseSwaggerUI(k => k.SwaggerEndpoint("v1/swagger.json", "My Api V1"));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
