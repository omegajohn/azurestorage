﻿using System;
using Microsoft.AspNetCore.Http;

namespace API.DTOs.Write
{
    public class AnimalWrite
    {
        public AnimalWrite()
        {
        }
        public string nombre { get; set; }
        public IFormFile foto { get; set; }
    }
}
