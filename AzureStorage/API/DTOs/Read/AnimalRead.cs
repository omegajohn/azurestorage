﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Read
{
    public class AnimalRead
    {
        public AnimalRead()
        {
            id = Guid.NewGuid().ToString();
        }
        [Key]
        public string id { get; set; }
        public string nombre { get; set; }
        public string foto { get; set; }
    }
}
