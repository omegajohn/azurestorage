﻿using System;
using System.Threading.Tasks;
using API.DTOs.Write;
using API.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AnimalController : Controller
    {
        private readonly IAnimal repo;
        public AnimalController(IAnimal collection)
        {
            repo = collection;
        }
        [HttpGet]
        public async Task<IActionResult> getAll()
        {
            try
            {
                return Ok(await repo.getAll());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return NoContent();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> getById(string id)
        {
            try
            {
                return Ok(await repo.getById(id));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> save([FromForm] AnimalWrite animal)
        {
            try
            {
                await repo.save(animal);
                return Created("Created", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return Created("Failed", false);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> update([FromForm] AnimalWrite animal, string id)
        {
            try
            {
                if (animal is null)
                {
                    return NotFound();
                }
                await repo.update(animal, id);
                return Created("Created", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return Created("Failed", false);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> delete(string id)
        {
            try
            {
                await repo.delete(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return NoContent();
        }
    }

    }
