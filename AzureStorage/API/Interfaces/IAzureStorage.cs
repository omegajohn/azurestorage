﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace API.Interfaces
{
    public interface IAzureStorage
    {
        Task deletePic(string path, string container);
        Task<string> savePic(string container, IFormFile file);
        Task<string> updatePic(string container, IFormFile file, string path);
    }
}
