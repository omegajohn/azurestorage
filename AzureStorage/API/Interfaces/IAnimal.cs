﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.DTOs.Read;
using API.DTOs.Write;

namespace API.Interfaces
{
    public interface IAnimal
    {
        Task<IEnumerable<AnimalRead>> getAll();
        Task<AnimalRead> getById(string id);
        Task save(AnimalWrite animal);
        Task update(AnimalWrite animal, string id);
        Task delete(string id);
    }
}
