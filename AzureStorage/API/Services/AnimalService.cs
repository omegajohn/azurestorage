﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.DML;
using API.DTOs.Read;
using API.DTOs.Write;
using API.Interfaces;
using API.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace API.Services
{
    public class AnimalService : IAnimal
    {
        private readonly IMapper mapper;
        private readonly DataContext db;
        private readonly IAzureStorage azure;
        private readonly string container = "animales";

        public AnimalService(IMapper map, DataContext context, IAzureStorage storage)
        {
            db = context;
            azure = storage;
            mapper = map;
        }

        public async Task delete(string id)
        {
            using (var ts = await db.Database.BeginTransactionAsync())
            {
                try
                {
                    var borrar = await db.Animales.FindAsync(id);
                    db.Remove(borrar);
                    await azure.deletePic(borrar.foto, container);
                    await db.SaveChangesAsync();
                    await ts.CommitAsync();

                }
                catch (Exception ex)
                {
                    await ts.RollbackAsync();
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        public async Task<IEnumerable<AnimalRead>> getAll()
        {
            try
            {
                var animales = await db.Animales.ToListAsync();
                return mapper.Map<IEnumerable<AnimalRead>>(animales);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public async Task<AnimalRead> getById(string id)
        {
            try
            {
                var animal = await db.Animales.FirstOrDefaultAsync(x => x.id == id);
                return mapper.Map<AnimalRead>(animal);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public async Task save(AnimalWrite animal)
        {
            using (var ts = await db.Database.BeginTransactionAsync())
            {
                try
                {
                    var ani = mapper.Map<Animal>(animal);
                    if (animal.foto is not null)
                    {
                        ani.foto = await azure.savePic(container, animal.foto);
                    }
                    await db.AddAsync(ani);
                    await db.SaveChangesAsync();
                    await ts.CommitAsync();
                }
                catch (Exception ex)
                {
                    await ts.RollbackAsync();
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        public async Task update(AnimalWrite animal, string id)
        {
            using (var ts = await db.Database.BeginTransactionAsync())
            {
                try
                {
                    var ani = await db.Animales.FirstOrDefaultAsync(x => x.id == id);
                        ani = mapper.Map(animal, ani);
                        if (animal.foto != null)
                        {
                            ani.foto = await azure.updatePic(container, animal.foto, ani.foto);
                        }
                        await db.SaveChangesAsync();
                        await ts.CommitAsync();
                }
                catch (Exception ex)
                {
                    await ts.RollbackAsync();
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
