﻿using System;
using System.IO;
using System.Threading.Tasks;
using API.Interfaces;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace API.Services
{
    public class AzureService : IAzureStorage
    {
        private readonly string azure;

        public AzureService(IConfiguration config)
        {
            azure = config.GetConnectionString("AzureStorage");
        }

        public async Task deletePic(string path, string container)
        {
            if (string.IsNullOrEmpty(path))
            {
                return;
            }
            var cliente = new BlobContainerClient(azure, container);
            await cliente.CreateIfNotExistsAsync();
            var file = Path.GetFileName(path);
            var blob = cliente.GetBlobClient(file);
            await blob.DeleteIfExistsAsync();
        }

        public async Task<string> savePic(string container, IFormFile file)
        {
            var cliente = new BlobContainerClient(azure, container);
            await cliente.CreateIfNotExistsAsync();
            cliente.SetAccessPolicy(PublicAccessType.Blob);
            var ext = Path.GetExtension(file.FileName);
            var name = $"{Guid.NewGuid()}{ext}";
            var blob = cliente.GetBlobClient(name);
            await blob.UploadAsync(file.OpenReadStream());
            return blob.Uri.ToString();
        }

        public async Task<string> updatePic(string container, IFormFile file, string path)
        {
            await deletePic(path, container);
            return await savePic(container, file);
        }
    }
}
