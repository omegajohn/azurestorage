﻿using System;
using API.Models;
using Microsoft.EntityFrameworkCore;

namespace API.DML
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> data) : base(data)
        {
        }

        public DbSet<Animal> Animales { get; set; }
    }
}
